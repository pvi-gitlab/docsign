Introduction
============

This file is meant to explain how to use setups with all options explained.

Setups are sets of parameters which define the behavior of docsign on each page.
With setups you can precisely change for each page : the position of your signature, its size, orientation..

You can also choose :
  - what pages to sign and what pages not to sign
  - wether you want a timestamp orientation
  - wether you wan a company/prsonalstamp or not

And you can pick pages by forwaord order or reverse order. 
This allos you to decide to only sign the last page, or the page before it, or only the third, or all the pages but the first...

Creating setups 
===============

Setups are created from simple text files like this one 

```bash
default_sign=1
default_scale="0.3"
default_x="7cm"
default_y="-12cm"
default_orientation="p"
```

You just need to create such a file, named for example : example.dsf

    .dsf extension is for "docsign stup file"

Then you can import this file :

```bash
docsign import example.dsf
```

After that the setup "example" willbe avaliableto sign documents like this :

```bash
docsign sign -s example my_document.pdf
```

Exporting setups
================

You can export setups like this :

```bash
docsign export example
```

This will create a file named example.dsf in the current directory

If you re-import it with the same file name it will override the exsisting setup with the corresponding name

If you rename the file and import it with a new name (modified ornot) it willcreate a new setup.


Content of setups
=================

A more complicated example of setup file will help to get the idea :

```bash
default_sign=1
default_datestamp=0
default_scale="0.3"
default_x="7cm"
default_y="-9.5cm"
default_orientation="p"

r1_sign=1
r1_scale="0.3"
r1_x="5cm"
r1_y="-2cm"
r1_orientation="p"
r1_datestamp=1

r0_sign=0
```

Each parameter name has 2parts : xxxx_yyyy

The first part xxxx targets the pages the 2nd part yyyy alters the behovior for the targeted page.

## Page targetting.

The part of the parameters names targetting pages can be :

  * **default** : targets all pages

  * **pZZZZ** where ZZZZ is a number (the number of the targeted page starting from the 1st one in forward order.). 
    pZZZ allows to target a page in forward order.
    p1 is the first page
    p2 is the 2nd page
    p3 is the 3rd
    and so on...

  * **rZZZZ*** where ZZZZ is a number (the number of the targeted page starting from the last and counting in reverse order)
    rZZZ allows to target a page in reverse order.
    rO is the last page
    r1 is the page before the last page
    r2 is the 2nd pages before the last
    r3 is the 3rd pages before the last
    and so on...

## 2nd part of parameters names.

  * **sign** : defines if the page willbe signed
  * **datestamp**  : defines if a date stamp will be added
  * **scale** : defines the scale factor to apply to the signature before applying it
  * **x** : defines the position of the signature horizontally starting from the center ofthe page (positive moves right, negative moves left)
  * **y** : defines the position of the signature vertically starting from the center ofthe page (positive moves up, negative moves down)
  * **orientation** : gives the orientation of the page (l = landscape, p = portrait)
  * ** stamp ** : defines which stamp will be used in addition to the signature
  * **preRotation** : gives the rotation to apply to the page befor signing (north, east, south, west, right, left, down)

## Explanation of the example setup 

```bash
default_sign=1               # <-- by default all pages will be signed
default_datestamp=0          # <-- by default no timestamp will be added
default_scale="0.3"          # <-- by default signature scale factor is 0.3
default_x="7cm"              # <-- by default signature is near the left side
default_y="-9.5cm"           # <-- by default signature is near the bottom
default_orientation="p"      # <-- by default orientation is portrait

r1_sign=1                    # <-- on the page before the last we still sign (useless parameter, default is already saying this)
r1_scale="0.3"               # <-- on the page before the last the signature scale factor is 0.3 (useless parameter, default is already saying this)
r1_x="5cm"                   # <-- on the page before the last the signature is morecentral than default
r1_y="-2cm"                  # <-- on the page before the last  the signature is morecentral than default
r1_orientation="p"           # <-- same oriendtation as default
r1_datestamp=1               # <-- we add a timestamp 

r0_sign=0                    # <-- the last page is not signed
```

