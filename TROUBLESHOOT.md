# Any problem ?


ImageMagick security policy 'PDF' blocking conversion
=====================================================

When trying to docsign add your signature, you might encounter this kind of error :

```sh
convert: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/408.
```
Don't panic, it's very simple to fix it.

It's due to the policies of ImageMagick, generally stored in /etc/ImageMagick-?/policy.xml file.

Edit your file (/etc/ImageMagick-[**your version**]/policy.xml) with your favorite text editor.

Find this line : 

```sh
<policy domain=t"coder" rights="none" pattern="{PS,PS2,PS3,EPS,PDF,XPS}" />
```

The line might differ slightly but the criteria here it that there are both rights="none" and PDF in the pattern.

The problem here is that overzealous policies prevent ImageMagick to write PDF files.

To solve this you need to remove completely the line, comment it (with xml comment : <!-- the line -->)  or adapt it to allow read + write privileges.

For example:
```sh
<policy domain=t"coder" rights="read|right" pattern="{PS,PS2,PS3,EPS,PDF,XPS}" />
```

More information here : [https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion](https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion)


Please contribute if you solve some other problems ;-)
===================================================== 
